#include <iostream>
#include <iomanip>
#include <fstream>
#include <cmath>
#include <complex>
#include <cstdio>
#include <ctime>
#include <cstdlib>
#include <omp.h>

using namespace std;

const double Err = 1.0e-6;//Relative error for Laplace transform inversion
const double pi = 4.0*atan(1.0);//defining the number "pi"

struct Transport{//Stucture for input parameters
  char model,inversion;
  int numlaplace,numtimes;
  double domain_length,position,initial_conc,injection_conc,pulse_duration;
  double total_porosity,logmobile_fraction,log_dispersivity,darcy_velocity;
  double log_omegaD0,logRm_less1, logRim_less1,halflife;
  double lowerbound,upperbound, statistic1, statistic2;//distribution parameters
};

void read_parameters(Transport &params)
{
  ifstream inp("input.txt");
  if(!inp){
    cerr<<"File could not be opened"<<endl;
    exit(1);}
  const int MAX=80;
  char buff[MAX];
  inp.getline(buff,MAX);//Read file header
  inp>>params.inversion;//"I" or "i" for inversion mode, other for forward
  inp>>params.model;//choose between double ("D" or "d"), multirate ("M" or "m") or single ("S" or "s") models
  inp>>params.numtimes;//Number of times at which to compute model output during inversion
  inp>>params.numlaplace;//Number of terms in de Hoog method
  inp>>params.domain_length;//Length of transport domain
  inp>>params.position;//Position at which breakthrough measured
  inp>>params.initial_conc;//Initial concentration in column
  inp>>params.injection_conc;//Injection concentration
  inp>>params.pulse_duration;//Duration of injection pulse
  inp>>params.total_porosity;//Total measured porosity
  inp>>params.logmobile_fraction;//Natural log of fraction of total porosity that constitutes mobile domain
  inp>>params.log_dispersivity;//Natural log of dispersivity
  inp>>params.darcy_velocity;//flux or Darcy velocity
  inp>>params.log_omegaD0;//Natural log of Damkohler number for double porosity model
  inp>>params.logRm_less1;//log(1-R_m), R_m is mobile domain retardation factor
  inp>>params.logRim_less1;//log(1-R_im), R_im is immobile domain retardation factor
  inp>>params.halflife;//Half-life of radionuclide in years
  inp>>params.lowerbound;//lower bound of the omegaD distribution
  inp>>params.upperbound;//upper bound of the omegaD distribution
  inp>>params.statistic1;//\mu (mean) in lognormal distribution
  inp>>params.statistic2;//\sigma (standard deviation) in lognormal distribution
}

void gauleg(int M,double x1, double x2, double *x, double *w)
{
  double EPS=1.0e-14;
  int m,j,i;
  double z1,z,xm,xl,pp,p3,p2,p1;

  int n=M;
  m=(n+1)/2;
  xm=0.5*(x2+x1);
  xl=0.5*(x2-x1);
  for (i=0;i<m;i++) {
    z=cos(pi*(i+0.75)/(n+0.5));
    do {
      p1=1.0;
      p2=0.0;
      for (j=0;j<n;j++) {
	p3=p2;
	p2=p1;
	p1=((2.0*j+1.0)*z*p2-j*p3)/(j+1);
      }
      pp=n*(z*p1-p2)/(z*z-1.0);
      z1=z;
      z=z1-p1/pp;
    } while (fabs(z-z1) > EPS);
    x[i]=xm-xl*z;
    x[n-1-i]=xm+xl*z;
    w[i]=2.0*xl/((1.0-z*z)*pp*pp);
    w[n-1-i]=w[i];
  }
}

complex<double> qgausBM(complex<double> func(double,complex<double>,Transport),complex<double> sL, double a, double b, Transport stats)
{
  //static const DP x[]={0.1488743389816312,0.4333953941292472,
  //	0.6794095682990244,0.8650633666889845,0.9739065285171717};
  //static const DP w[]={0.2955242247147529,0.2692667193099963,
  //	0.2190863625159821,0.1494513491505806,0.0666713443086881};
  int j;
  const int M=200;
  double xr,xm,dx,x[M/2], w[M/2];
  complex<double> s;
	  
  double xx[M], ww[M];
  double x1 = 1.0;
  double x2 = -1.0;
	
  gauleg(M,x1,x2,xx,ww);	
  int n=0;
  for(int i=M/2 -1;i>=0;i--){
    x[n]=xx[i];
    w[n]=fabs(ww[i]);
    n++;}
	  
  xm=0.5*(b+a);
  xr=0.5*(b-a);
  s=0;
  for (j=0;j<M/2;j++) {
    dx=xr*x[j];
    s += w[j]*(func(xm+dx,sL,stats)+func(xm-dx,sL,stats));
  }
  return s *= xr;
}

double distbn(double x,Transport stats)
{
  double u,v,param1,param2,param3,param4;
  param4 = stats.upperbound;//upper bound
  param3 = stats.lowerbound;//lower bound
  param2 = stats.statistic2;//variance
  param1 = stats.statistic1;//mean

  //Truncated lognormal distribution
  v = 1/(1/x - 1/param4) - param3;
  u = (1/(v*abs(param2)*sqrt(2*pi)))*exp(-pow((log(v)-param1)/param2,2.0)/2.0);
  
  return u;
}

double expdistbn(double x,Transport stats)
{
  double u,v,param1,param2,param3,param4;
  param4 = stats.upperbound;//upper bound
  param3 = stats.lowerbound;//lower bound
  param2 = stats.statistic2;//variance
  param1 = stats.statistic1;//mean

  //Truncated lognormal distribution
  v = 1/(1/x - 1/param4) - param3;
  u = exp(-(x-param1)/param2);
  
  if(x>param1)
    return u/param2;
  else
    return 0.0;
}

complex<double> func(double x,complex<double> sL, Transport stats)
{
  //memory function;
//  return ((x/(sL+x))*distbn(x,stats));
  return ((x/(sL+x))*expdistbn(x,stats));
}

complex<double> memf(complex<double> func(double, complex<double>, Transport), complex<double> sL, Transport stats)
{
  double a,b;
  a = stats.lowerbound;
  b = stats.upperbound;

  return qgausBM(func,sL,a,b,stats); //Integration of memory kernel with Gauss-Legendre quadrature
}

complex<double> lap_cDinj(complex<double> sL, double tDinj, double cDinj)
{
  return cDinj*(1.0-exp(-sL*tDinj))/sL;
}

complex<double> lap_cD_1Dx2(complex<double> func(double, complex<double>, Transport),complex<double>s,double xD, double betaT0, double betaT,double param0,double Lambda,double cD0,double cDinj,double tDinj,double Lc,double A,double B,double P, char ch,Transport params)
{
  //Laplace transform of concentration for 1D-x flow
  //The transfer coefficients are continuous random variables
  complex<double> sL,g0,g1,f,u,cDp,CD0;
  
  sL = Lambda + s;
  if(ch == 's' || ch == 'S'){
    g0 = 0.0;
    g1 = 0.0;
  }else if(ch == 'd' || ch == 'D'){
    g0 = betaT0*param0/(sL + param0);
    g1 = 0.0;
  }else{
    g0 = betaT0*param0/(sL + param0);
    g1 = betaT*memf(func,sL,params);
  }
  
  f = sL*(1.0 + g0 + g1);
  cDp = cD0/sL;
  u = P*sqrt(1.0 + 4.0*f/P);
  CD0 = (lap_cDinj(sL,tDinj,cDinj) - B*cDp)/(B+A*(P-u)/(2.0*Lc));
  
  return (CD0*exp(xD*(P-u)/2.0) + cDp);
}

complex<double> lambda(int k,double tmax)
{//Computes the Laplace parameter given k
 //ch = Specifier for real or imaginary part of lambda
  double T;
  T = 0.8*tmax;
  complex<double> Lmd(-log(Err)/(2*T),k*pi/T);
  return Lmd;
}

double deHoog(double t,double tmax,int Nk,complex<double> *HL)
{//Performs the Laplace transform inversion
 //Implementation of method of De Hoog et al. (1982)
 //HL[][] matrix containing the power series coefficients
  int M2= Nk/2;
  double T,beta,hL;
  complex<double> z,*a,*q1, *eps1,*eps2,*q2,*d,*A,*B;

  T = 0.8*tmax;
  beta = -log(Err)/(2*T);
  complex<double> alpha(0,pi*t/T);
  z = exp(alpha);
  a = new complex<double>[Nk+1];
  d = new complex<double>[Nk+1];
  eps1 = new complex<double>[Nk+1];
  q1 = new complex<double>[Nk];
  A = new complex<double>[Nk+2];
  B = new complex<double>[Nk+2];

  for(int i=0;i<=Nk;i++){
    if(i==0){
      a[i]=HL[i]/2.0;}
    else{
      a[i]=HL[i];}
    eps1[i]=complex<double>(0.0,0.0);}
  for(int i=0;i<Nk;i++){
    q1[i]=a[i+1]/a[i];}
  d[0] = a[0];
  d[1] = -q1[0];
  for(int r=1;r<=M2;r++){
    eps2 = new complex<double>[Nk-2*r+1];
    for(int i=0;i<=(Nk-2*r);i++){
      eps2[i]=q1[i+1]-q1[i]+eps1[i+1];}
    q2 = new complex<double>[Nk-2*r];
    for(int i=0;i<(Nk-2*r);i++){
      q2[i]=q1[i+1]*eps2[i+1]/eps2[i];}
    d[2*r]=-eps2[0];
    if(r<M2){
      d[2*r+1]=-q2[0];}
    eps1 = new complex<double>[Nk-2*r+1];
    q1	 = new complex<double>[Nk-2*r];
    for(int i=0;i<=(Nk-2*r);i++){
      eps1[i]=eps2[i];
      if(i<(Nk-2*r)){
        q1[i]=q2[i];}}
  }
  A[0] = complex<double>(0.0,0.0);
  A[1] = d[0];
  B[0] = complex<double>(1.0,0.0);
  B[1] = complex<double>(1.0,0.0);
  for(int i=2;i<=(Nk+1);i++){
    A[i] = A[i-1]+d[i-1]*z*A[i-2];
    B[i] = B[i-1]+d[i-1]*z*B[i-2];}
  hL = exp(beta*t)*(real(A[Nk+1]/B[Nk+1]))/T;
  // cout<<hL<<endl;

  return hL;
}

double lap_invert(complex<double> func(double, complex<double>, Transport),double tD,double xD, double betaT0, double betaT,double param0,double Lambda,double cD0,double cDinj,double tDinj,double Lc,double A,double B,double P, char ch,Transport params)
{
  const int M = 12;
  complex<double> p,v[M];
  double max_tD;
  double T;
   
  max_tD = 1.5*tD;
  #pragma omp parallel for private(p)
  for(int n=0;n<M;n++){
    p = lambda(n,max_tD);
    v[n] = lap_cD_1Dx2(func,p,xD,betaT0, betaT,param0,Lambda,cD0,cDinj,tDinj,Lc,A,B,P, ch,params);
  }
  return deHoog(tD,max_tD,M,v);
}

int main()
{
  int eta=2;
  double sD,*tD,tDinj,Tc,xD,D_l,Vx,Lc,A,B,P,*cD,ccD,cD0,Lambda,ww;
  double R_m,phi_m,R_im,phi_im,phi_im0,phi_im1,betaT0,betaT,omegaD0,cDinj;
  double alphaL;
  char ch;
  const double log2 = 0.69314718056;

  //  ifstream inp("besJ0zeros.dat");
  ifstream inp("times.txt");//File of times in minutes for drawdown observations
  ofstream outf("output.txt",ios::out);
  outf<<setiosflags(ios::fixed)
      <<setiosflags(ios::showpoint)
      <<setprecision(6);
 
  Transport params;
  read_parameters(params);
  ch = params.model;

  if(ch == 'm' || ch == 'M')
    ww = 0.0;//for multirate model
  else
    ww = 1.0;//for double and single porosity models

  double phi_T = params.total_porosity; //Total measured porosity
  phi_m = exp(params.logmobile_fraction)*phi_T; //Mobile phase porosity
  phi_im = phi_T-phi_m; //Immobile phase porosity
  phi_im0 = ww*phi_im; //Immobile phase porosity
  phi_im1 = (1-ww)*phi_im; //Immobile phase porosity
  alphaL = exp(params.log_dispersivity); //Dispersivity
  omegaD0 = exp(params.log_omegaD0); //Dual-porosity mass transfer coefficient

  double c_0 = params.initial_conc; //Initial concentration
  double c_inj = params.injection_conc; //Injection concentration
  double t_inj = params.pulse_duration; //Duration of injection spike in hours

  double x = params.position; //Observation point distance from injection point (m)
  cD0 = c_0/c_inj; //Initial concentration
  cDinj = c_inj/c_inj; //Normalized Injection concentration
  Vx = params.darcy_velocity/phi_m; //Advective velocity (m/s)
  D_l = alphaL*Vx; //Disperion coefficient
  R_m = 1.0+exp(params.logRm_less1); //Mobile phase retardation factor
  R_im = 1.0+exp(params.logRim_less1); //Immobile phase retardation factor
  Lc = params.domain_length; //characteristic length scale
  xD = x/Lc; //Nondimensional observation point
  Tc = R_m*Lc/Vx; //Characteristic time
  A = -D_l/Vx; //Boundary condition constant 1
  B = 1.0; //Boundary condition constant 2
  P = Vx*Lc/D_l; //Peclet number
  Lambda = log2*Tc/(params.halflife*3600*365); //Dimesionless 1st-order radioactive decay constant
  betaT0 = R_im*phi_im0/(R_m*phi_m); //Dual-porosity capacity ratio
  betaT = R_im*phi_im1/(R_m*phi_m); //Multirate transfer capacity ratio

  int Nk = params.numlaplace; //Number of parameters in de Hoog Laplace transform inversion
  tDinj = t_inj*3600.0/Tc; //Dimensionless duration of tracer injection
  if(params.inversion == 'I' || params.inversion == 'i'){//Run following block for parameter estimation
    tD = new double[params.numtimes];//Setting size of array of dimensionless times
    cD = new double[params.numtimes];//Setting size of array of dimensionless times
    outf<<"MODEL OUTPUT FILE"<<endl;
    outf<<"Calculated concentrations"<<endl;
    for(int n=0;n<params.numtimes;n++){
      inp>>tD[n];//Read times in minutes from file times.dat
      tD[n] /= Tc; //Convert times to seconds then to dimensionless times
      cD[n] = lap_invert(func,tD[n],xD,betaT0,betaT,omegaD0,Lambda,cD0,cDinj,tDinj,Lc,A,B, P, ch, params);
      outf<<setw(10)<<c_inj*cD[n]<<endl;
    }
       // for(int n=0;n<params.numtimes;n++){
         // outf<<setw(10)<<log(c_inj*cD[n])<<endl;
        //}
  }else{//Run following block for forward simulation
    double t =1e3;
    double dt = 100;
    double tmax = 1e4;
    double tD2;
    for(int i=0;i<4;i++){
      // cout<<i<<endl;
      while(t<tmax){
        //inp>>t;
        tD2 = t/Tc; //Dimensionless time
        //cout<<tD2<<endl;
        ccD = lap_invert(func,tD2,xD,betaT0,betaT,omegaD0,Lambda,cD0,cDinj,tDinj,Lc,A,B, P, ch, params);
        outf<<tD2<<"\t"<<t<<"\t"<<ccD<<endl;
        t+=dt;
      }
      dt *= 10.0;
      tmax *= 10.0;
    }
  }
	
  return 0;
}
