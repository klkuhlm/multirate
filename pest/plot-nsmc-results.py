import pylab
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.font_manager as fm
from glob import glob

legfm = fm.FontProperties(size='medium')

nrzn = 500
SEC2DAYS =  1.0/(60.0*60.0*24.0)

dirs = glob('singleB8')
dirs.extend(glob('doubleB8'))
dirs.extend(glob('multirate2B8'))

numpar = 7

par = np.zeros((nrzn,numpar))
basepar = np.zeros((3,numpar))

for d in ['multirate2B8-bounds']:
    
    if 'B4B5B8' in d:
        continue

    print d

    test = d.lstrip('single').lstrip('double').lstrip('multirate2').rstrip('-bounds')

    if test == 'B1':
        tracer = '3H'
    elif test[-1] == 'b':
        tracer = '232U'
        test = test[:-1]
    elif test[-1] == 'a':
        tracer = '22Na'
        test = test[:-1]
    else:
        tracer =  '22Na'

    fh = open(d+'/input.txt','r')
    lines = [x.strip() for x in fh.readlines()]
    fh.close()
    nobs = int(lines[3])

    phi = np.zeros((nrzn,))
    calc = np.zeros((nrzn,nobs))
    residual = np.zeros((nrzn,nobs))


    params = {'backend': 'ps',
              'axes.labelsize': 22,
              'text.fontsize': 11,
              'legend.fontsize': 22,
              'xtick.labelsize': 22,
              'ytick.labelsize': 22,
              'text.usetex': True} #width,height
    pylab.rcParams.update(params)

    fig = plt.figure(1)
    #ax = fig.add_subplot(211)
    ax = fig.add_subplot(111)
     
    # observed data
    t,c = np.loadtxt(test + '_' + tracer + '.txt',unpack=True)
    t *= SEC2DAYS

    nobs = min(nobs,t.shape[0])

    modelt = np.loadtxt('B8-times-longer.txt')*SEC2DAYS
    
    if 'multi' in d:
        baserec = file('%s/%s.rec' % (d,d),'r')
        baselines = [line.strip() for line in baserec.readlines()]
        print 'baselines',len(baselines)
        try:
            idx = baselines.index('Adjustable parameters ----->') + 4
        except ValueError:
            idx = lines.index('Parameters ----->') + 3
        basepar[0,:] = np.array([float(line.split()[1]) for line in baselines[idx:idx+numpar]]) # optimum
        #basepar[1,:] = np.array([float(line.split()[2]) for line in baselines[idx:idx+numpar]]) # -95%
        #basepar[2,:] = np.array([float(line.split()[3]) for line in baselines[idx:idx+numpar]]) # +95%

    for j in range(nrzn):
                
        root = d + '/' + d + '-nsmc-%i' % (j+1,)
        try:
            fh = open(root + '.rec','r')
        except:
            continue
        print root
        lines = [x.strip() for x in fh.readlines()]
        fh.close()

        n = int(lines[13].split(':')[1])

        if 'multi' in d:
	    try:
                idx = lines.index('Adjustable parameters ----->') + 4
            except ValueError:
	        idx = lines.index('Parameters ----->') + 3

            par[j,:] = np.array([float(line.split()[1]) for line in lines[idx:idx+numpar]])	    
            
        idx = lines.index('Objective function ----->')
        idx += 2
        phi[j] = float(lines[idx].split('=')[1])
        
        idx = lines.index('Observations ----->')
        idx += 4
        
        calc[j,:n] = np.array([float(line.split()[2]) 
                               for line in lines[idx:idx+n]])
        
        residual[j,:n] = np.array([float(line.split()[3]) 
                               for line in lines[idx:idx+n]])
        
        residual[j,n:] = -999.
        
        modelval = np.loadtxt('%s/output1-%s-%i.txt' % (d,d,j+1),skiprows=2)

	m = np.abs(calc[j,:n]) > 1.0  
        calc[j,:n][m] = np.NaN

        #ax.loglog(t[:n],calc[j,:n],'k-',linewidth=0.25)
        #ax.plot(t[:n],calc[j,:n],'k-',linewidth=0.25)

        #ax.plot(modelt,modelval,'k-',linewidth=0.1)
        ax.semilogy(modelt,modelval,'k-',linewidth=0.1)
        #ax.loglog(modelt,modelval,'k-',linewidth=0.1)

    BB = 0.475
    #ax.plot(t,c,'r.')
    ax.semilogy(t,c,'r.')
    #ax.loglog(t,c,'r.')

    #ax.plot(modelt,BB*modelt**(-2.5),'g--')
    dashed = ax.semilogy(modelt,BB*modelt**(-2.5),'g--',linewidth=2)[0]
    dashed.set_dashes((20,5))
    #ax.loglog(modelt,BB*modelt**(-2.5),'g--')
    
    BB = 0.125
    dotted = ax.semilogy(modelt,BB*modelt**(-1.5),'b:',dashes=(10,5),linewidth=2)[0]
    dotted.set_dashes((5,5))
    ax.set_xlabel('time (days)')
    ax.set_ylabel('c ($\mu$Ci/ml)')
    ax.set_ylim([1e-4,0.14])
    ax.annotate('-5/2 slope',xy=(4.8,0.01),xytext=(2.0, 0.003),fontsize=20,
                arrowprops=dict(facecolor='black', shrink=0.05, headwidth=6, width=0.5))
    ax.annotate('-3/2 slope',xy=(2.3,0.035),xytext=(1.0, 0.01),fontsize=20,
                arrowprops=dict(facecolor='black', shrink=0.05, headwidth=6, width=0.5))
    #ax.set_title(d.strip('B8'))
    #ax.annotate(d.strip('2B8'), xy=(0.5, 0.13),  xycoords='data',
     #           xytext=(0.5, 0.1), textcoords='offset points',
      #          arrowprops=None
       #         )
    #t = ax.text(0.5, 0.13, d.strip('2B8'), ha="center", va="center", rotation=0,
    #        size=22)

    #ax = fig.add_subplot(212)
    #ax.hist(phi)
    #ax.set_xlabel('SOSR')
    #ax.set_ylabel('frequency')

    plt.savefig('nsmc-results/' + d + 'd.eps')
    plt.close(1)
    
    plt.figure(2)
    plt.subplot(111)
    plt.subplots_adjust(bottom=0.15)
    n = 268
    #print residual[:,:n].mean(),residual[:,:n].min(),residual[:,:n].max(),residual[:,:n].var()
    plt.hist(residual[:,n]*1000,90,range=(-6,10),label=d.rstrip('B8').rstrip('2'))
    #plt.plot(residual[:,n])
    plt.title('residuals at %.1f days' % t[n])
    plt.xlabel('residual ($\\times 10^{-3}$ $\\mu Ci/ml$)')
    plt.ylabel('frequency')
    plt.legend(loc=0,prop=legfm)
    plt.savefig('nsmc-results/error-hist-b.eps' )

plt.figure(3,figsize=(11,8))
variables = ['$c_{\mathrm{inj}}$ ($\\mu$Ci/ml)','$t_{\mathrm{inj}}$ (hrs)',
             '$\\phi_{\mathrm{m}}$','$\\alpha$ (cm)','$\\mu$','$\\sigma$',
             '$\\hat{\\omega}_\mathrm{max}$']
par[:,2] = np.exp(par[:,2])*0.14
#basepar[:,2]  = np.exp(basepar[:,2])*0.14
par[:,3] = np.exp(par[:,3])*100
#basepar[:,3] = np.exp(basepar[:,3])*100
for j in range(numpar):
    plt.subplot(4,2,j+1)
    print par[:,j].min(),par[:,j].max(),par[:,j].mean()
    plt.hist(par[:,j],50)
    plt.annotate('$\\mu=$%.3g' % par[:,j].mean(),xy=(0.7,0.8),xycoords='axes fraction')
    plt.annotate('$\\sigma^2=$%.3g' % par[:,j].var(),xy=(0.7,0.675),xycoords='axes fraction')
    plt.axvline(basepar[0,j],color='red')
    #plt.axvline(basepar[1,j],color='green')
    #plt.axvline(basepar[2,j],color='green')
    plt.xlabel(variables[j])
    plt.yticks(size='medium')
    plt.xticks(size='medium')
    if j == 0 or j == 2 or j == 4:
        plt.ylabel('frequency')
plt.subplots_adjust(bottom=0.15,wspace=0.15,hspace=0.35)
plt.savefig('nsmc-results/multirateB8-parameter-histogram.eps')    
plt.close(3)

    #np.save('nsmc-results/'+d,np.concatenate((phi[:,None],calc),axis=1))
        
