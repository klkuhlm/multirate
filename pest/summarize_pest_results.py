from glob import glob

# calibration results
results = {'single':{},
           'double':{},
           'multirate':{}}

for d in glob('singleB*'):

    test = d[6:] # drop 'single' part
    print test
    results['single'][test] = {}

    fh = open('%s/%s.par', (d,d),'r')
    lines = fh.readlines()
    npars = len(lines)-1 # one header
    for j in range(npars):
        fields = lines[j+1].split()
        results['single'][test][fields[0]] = float(fields[1])
    
    
# NSMC parameter distributions
