
 MODEL OUTPUTS AT END OF OPTIMISATION ITERATION NO.   7:-


 Name                 Group          Measured         Modelled         Residual         Weight
 p001                 obsgroup      2.3700000E-05    2.5000000E-05   -1.3000000E-06     1.000000    
 p002                 obsgroup      5.8300000E-05    2.5000000E-05    3.3300000E-05     1.000000    
 p003                 obsgroup      2.7200000E-05    2.5000000E-05    2.2000000E-06     1.000000    
 p004                 obsgroup      2.2800000E-05    2.5000000E-05   -2.2000000E-06     1.000000    
 p005                 obsgroup      6.6400000E-05    2.5000000E-05    4.1400000E-05     1.000000    
 p006                 obsgroup      3.1000000E-05    2.5000000E-05    6.0000000E-06     1.000000    
 p007                 obsgroup      2.7600000E-05    2.5000000E-05    2.6000000E-06     1.000000    
 p008                 obsgroup      2.4800000E-05    2.5000000E-05   -2.0000000E-07     1.000000    
 p009                 obsgroup      2.7900000E-05    2.5000000E-05    2.9000000E-06     1.000000    
 p010                 obsgroup      2.9700000E-05    2.5000000E-05    4.7000000E-06     1.000000    
 p011                 obsgroup      2.7400000E-05    2.5000000E-05    2.4000000E-06     1.000000    
 p012                 obsgroup      2.9600000E-05    2.5000000E-05    4.6000000E-06     1.000000    
 p013                 obsgroup      2.7600000E-05    2.5000000E-05    2.6000000E-06     1.000000    
 p014                 obsgroup      2.7600000E-05    2.5000000E-05    2.6000000E-06     1.000000    
 p015                 obsgroup      2.5800000E-05    2.6000000E-05   -2.0000000E-07     1.000000    
 p016                 obsgroup      2.8400000E-05    2.7000000E-05    1.4000000E-06     1.000000    
 p017                 obsgroup      2.4600000E-05    2.9000000E-05   -4.4000000E-06     1.000000    
 p018                 obsgroup      2.9600000E-05    3.3000000E-05   -3.4000000E-06     1.000000    
 p019                 obsgroup      2.1000000E-05    3.9000000E-05   -1.8000000E-05     1.000000    
 p020                 obsgroup      2.9500000E-05    4.9000000E-05   -1.9500000E-05     1.000000    
 p021                 obsgroup      2.2900000E-05    6.3000000E-05   -4.0100000E-05     1.000000    
 p022                 obsgroup      2.6900000E-05    8.3000000E-05   -5.6100000E-05     1.000000    
 p023                 obsgroup      2.8100000E-05    1.1000000E-04   -8.1900000E-05     1.000000    
 p024                 obsgroup      2.0000000E-05    1.4400000E-04   -1.2400000E-04     1.000000    
 p025                 obsgroup      2.8300000E-05    1.8600000E-04   -1.5770000E-04     1.000000    
 p026                 obsgroup      3.1800000E-05    2.3800000E-04   -2.0620000E-04     1.000000    
 p027                 obsgroup      5.4400000E-05    3.0100000E-04   -2.4660000E-04     1.000000    
 p028                 obsgroup      3.4600000E-05    3.7500000E-04   -3.4040000E-04     1.000000    
 p029                 obsgroup      2.8400000E-05    4.5900000E-04   -4.3060000E-04     1.000000    
 p030                 obsgroup      6.1400000E-05    5.5400000E-04   -4.9260000E-04     1.000000    
 p031                 obsgroup      1.8850000E-04    6.6000000E-04   -4.7150000E-04     1.000000    
 p032                 obsgroup      5.7600000E-05    1.2100000E-03   -1.1524000E-03     1.000000    
 p033                 obsgroup      8.0200000E-05    1.3650000E-03   -1.2848000E-03     1.000000    
 p034                 obsgroup      8.0200000E-05    1.5360000E-03   -1.4558000E-03     1.000000    
 p035                 obsgroup      1.0300000E-04    1.7190000E-03   -1.6160000E-03     1.000000    
 p036                 obsgroup      1.4730000E-04    1.9100000E-03   -1.7627000E-03     1.000000    
 p037                 obsgroup      2.1320000E-04    2.1040000E-03   -1.8908000E-03     1.000000    
 p038                 obsgroup      2.5030000E-04    2.3060000E-03   -2.0557000E-03     1.000000    
 p039                 obsgroup      3.6670000E-04    2.5160000E-03   -2.1493000E-03     1.000000    
 p040                 obsgroup      4.9540000E-04    2.7280000E-03   -2.2326000E-03     1.000000    
 p041                 obsgroup      5.7680000E-04    2.9430000E-03   -2.3662000E-03     1.000000    
 p042                 obsgroup      8.0860000E-04    3.1630000E-03   -2.3544000E-03     1.000000    
 p043                 obsgroup      9.8160000E-04    3.3860000E-03   -2.4044000E-03     1.000000    
 p044                 obsgroup      1.0815000E-03    3.6070000E-03   -2.5255000E-03     1.000000    
 p045                 obsgroup      1.4008000E-03    3.8240000E-03   -2.4232000E-03     1.000000    
 p046                 obsgroup      1.6583000E-03    4.0530000E-03   -2.3947000E-03     1.000000    
 p047                 obsgroup      1.9158000E-03    4.2870000E-03   -2.3712000E-03     1.000000    
 p048                 obsgroup      2.3072000E-03    4.5120000E-03   -2.2048000E-03     1.000000    
 p049                 obsgroup      2.4617000E-03    4.7360000E-03   -2.2743000E-03     1.000000    
 p050                 obsgroup      2.8840000E-03    4.9580000E-03   -2.0740000E-03     1.000000    
 p051                 obsgroup      3.0900000E-03    5.1690000E-03   -2.0790000E-03     1.000000    
 p052                 obsgroup      3.6359000E-03    5.3760000E-03   -1.7401000E-03     1.000000    
 p053                 obsgroup      3.9346000E-03    5.5970000E-03   -1.6624000E-03     1.000000    
 p054                 obsgroup      5.0470000E-03    5.8220000E-03   -7.7500000E-04     1.000000    
 p055                 obsgroup      5.2530000E-03    6.0340000E-03   -7.8100000E-04     1.000000    
 p056                 obsgroup      5.5826000E-03    6.2380000E-03   -6.5540000E-04     1.000000    
 p057                 obsgroup      5.8504000E-03    6.4380000E-03   -5.8760000E-04     1.000000    
 p058                 obsgroup      6.4787000E-03    6.6310000E-03   -1.5230000E-04     1.000000    
 p059                 obsgroup      6.6744000E-03    6.8190000E-03   -1.4460000E-04     1.000000    
 p060                 obsgroup      7.1688000E-03    7.0080000E-03    1.6080000E-04     1.000000    
 p061                 obsgroup      7.3645000E-03    7.1990000E-03    1.6550000E-04     1.000000    
 p062                 obsgroup      7.5396000E-03    7.3880000E-03    1.5160000E-04     1.000000    
 p063                 obsgroup      8.2606000E-03    7.5670000E-03    6.9360000E-04     1.000000    
 p064                 obsgroup      8.3739000E-03    7.7350000E-03    6.3890000E-04     1.000000    
 p065                 obsgroup      8.7447000E-03    7.8970000E-03    8.4770000E-04     1.000000    
 p066                 obsgroup      9.1155000E-03    8.0550000E-03    1.0605000E-03     1.000000    
 p067                 obsgroup      9.5069000E-03    8.2070000E-03    1.2999000E-03     1.000000    
 p068                 obsgroup      9.5893000E-03    8.3530000E-03    1.2363000E-03     1.000000    
 p069                 obsgroup      9.8777000E-03    8.4920000E-03    1.3857000E-03     1.000000    
 p070                 obsgroup      1.0186700E-02    8.6230000E-03    1.5637000E-03     1.000000    
 p071                 obsgroup      1.0403000E-02    8.7500000E-03    1.6530000E-03     1.000000    
 p072                 obsgroup      1.0918000E-02    8.8760000E-03    2.0420000E-03     1.000000    
 p073                 obsgroup      1.1021000E-02    8.9960000E-03    2.0250000E-03     1.000000    
 p074                 obsgroup      1.1021000E-02    9.1090000E-03    1.9120000E-03     1.000000    
 p075                 obsgroup      1.1330000E-02    9.2190000E-03    2.1110000E-03     1.000000    
 p076                 obsgroup      1.1433000E-02    9.3230000E-03    2.1100000E-03     1.000000    
 p077                 obsgroup      1.1742000E-02    9.4220000E-03    2.3200000E-03     1.000000    
 p078                 obsgroup      1.1227000E-02    9.5150000E-03    1.7120000E-03     1.000000    
 p079                 obsgroup      1.1845000E-02    9.6020000E-03    2.2430000E-03     1.000000    
 p080                 obsgroup      1.1742000E-02    9.6860000E-03    2.0560000E-03     1.000000    
 p081                 obsgroup      1.2154000E-02    9.7630000E-03    2.3910000E-03     1.000000    
 p082                 obsgroup      1.1536000E-02    9.8360000E-03    1.7000000E-03     1.000000    
 p083                 obsgroup      1.1639000E-02    9.9060000E-03    1.7330000E-03     1.000000    
 p084                 obsgroup      1.1639000E-02    9.9720000E-03    1.6670000E-03     1.000000    
 p085                 obsgroup      1.1639000E-02    1.0036000E-02    1.6030000E-03     1.000000    
 p086                 obsgroup      1.2154000E-02    1.0094000E-02    2.0600000E-03     1.000000    
 p087                 obsgroup      1.2257000E-02    1.0147000E-02    2.1100000E-03     1.000000    
 p088                 obsgroup      1.1948000E-02    1.0194000E-02    1.7540000E-03     1.000000    
 p089                 obsgroup      1.2154000E-02    1.0238000E-02    1.9160000E-03     1.000000    
 p090                 obsgroup      1.1845000E-02    1.0278000E-02    1.5670000E-03     1.000000    
 p091                 obsgroup      1.1639000E-02    1.0313000E-02    1.3260000E-03     1.000000    
 p092                 obsgroup      1.1536000E-02    1.0346000E-02    1.1900000E-03     1.000000    
 p093                 obsgroup      1.1639000E-02    1.0377000E-02    1.2620000E-03     1.000000    
 p094                 obsgroup      1.1639000E-02    1.0403000E-02    1.2360000E-03     1.000000    
 p095                 obsgroup      1.1330000E-02    1.0425000E-02    9.0500000E-04     1.000000    
 p096                 obsgroup      1.1021000E-02    1.0443000E-02    5.7800000E-04     1.000000    
 p097                 obsgroup      1.1433000E-02    1.0458000E-02    9.7500000E-04     1.000000    
 p098                 obsgroup      1.1330000E-02    1.0470000E-02    8.6000000E-04     1.000000    
 p099                 obsgroup      1.1124000E-02    1.0479000E-02    6.4500000E-04     1.000000    
 p100                 obsgroup      1.1124000E-02    1.0486000E-02    6.3800000E-04     1.000000    
 p101                 obsgroup      1.0815000E-02    1.0489000E-02    3.2600000E-04     1.000000    
 p102                 obsgroup      1.0815000E-02    1.0490000E-02    3.2500000E-04     1.000000    
 p103                 obsgroup      1.0815000E-02    1.0489000E-02    3.2600000E-04     1.000000    
 p104                 obsgroup      1.0712000E-02    1.0485000E-02    2.2700000E-04     1.000000    
 p105                 obsgroup      1.0403000E-02    1.0478000E-02   -7.5000000E-05     1.000000    
 p106                 obsgroup      1.0609000E-02    1.0469000E-02    1.4000000E-04     1.000000    
 p107                 obsgroup      1.0403000E-02    1.0458000E-02   -5.5000000E-05     1.000000    
 p108                 obsgroup      1.0094000E-02    1.0445000E-02   -3.5100000E-04     1.000000    
 p109                 obsgroup      1.0403000E-02    1.0430000E-02   -2.7000000E-05     1.000000    
 p110                 obsgroup      9.6614000E-03    1.0413000E-02   -7.5160000E-04     1.000000    
 p111                 obsgroup      9.7438000E-03    1.0393000E-02   -6.4920000E-04     1.000000    
 p112                 obsgroup      9.4245000E-03    1.0373000E-02   -9.4850000E-04     1.000000    
 p113                 obsgroup      9.3318000E-03    1.0352000E-02   -1.0202000E-03     1.000000    
 p114                 obsgroup      9.7026000E-03    1.0328000E-02   -6.2540000E-04     1.000000    
 p115                 obsgroup      9.4142000E-03    1.0301000E-02   -8.8680000E-04     1.000000    
 p116                 obsgroup      9.7232000E-03    1.0273000E-02   -5.4980000E-04     1.000000    
 p117                 obsgroup      9.3421000E-03    1.0244000E-02   -9.0190000E-04     1.000000    
 p118                 obsgroup      9.4554000E-03    1.0213000E-02   -7.5760000E-04     1.000000    
 p119                 obsgroup      9.3421000E-03    1.0181000E-02   -8.3890000E-04     1.000000    
 p120                 obsgroup      9.2082000E-03    1.0146000E-02   -9.3780000E-04     1.000000    
 p121                 obsgroup      9.1876000E-03    1.0110000E-02   -9.2240000E-04     1.000000    
 p122                 obsgroup      8.8065000E-03    1.0072000E-02   -1.2655000E-03     1.000000    
 p123                 obsgroup      8.7344000E-03    9.9910000E-03   -1.2566000E-03     1.000000    
 p124                 obsgroup      8.4872000E-03    9.9070000E-03   -1.4198000E-03     1.000000    
 p125                 obsgroup      8.2709000E-03    9.8200000E-03   -1.5491000E-03     1.000000    
 p126                 obsgroup      8.3842000E-03    9.7270000E-03   -1.3428000E-03     1.000000    
 p127                 obsgroup      8.2091000E-03    9.6320000E-03   -1.4229000E-03     1.000000    
 p128                 obsgroup      8.3739000E-03    9.5850000E-03   -1.2111000E-03     1.000000    
 p129                 obsgroup      7.8589000E-03    9.4880000E-03   -1.6291000E-03     1.000000    
 p130                 obsgroup      7.6529000E-03    9.3880000E-03   -1.7351000E-03     1.000000    
 p131                 obsgroup      7.5808000E-03    9.2840000E-03   -1.7032000E-03     1.000000    
 p132                 obsgroup      7.4881000E-03    9.1820000E-03   -1.6939000E-03     1.000000    
 p133                 obsgroup      7.4984000E-03    9.0750000E-03   -1.5766000E-03     1.000000    
 p134                 obsgroup      7.1688000E-03    8.9640000E-03   -1.7952000E-03     1.000000    
 p135                 obsgroup      7.3542000E-03    8.7440000E-03   -1.3898000E-03     1.000000    
 p136                 obsgroup      6.9216000E-03    8.6300000E-03   -1.7084000E-03     1.000000    
 p137                 obsgroup      7.0040000E-03    8.5030000E-03   -1.4990000E-03     1.000000    
 p138                 obsgroup      6.6332000E-03    8.3850000E-03   -1.7518000E-03     1.000000    
 p139                 obsgroup      6.8289000E-03    8.2670000E-03   -1.4381000E-03     1.000000    
 p140                 obsgroup      6.2727000E-03    8.1480000E-03   -1.8753000E-03     1.000000    
 p141                 obsgroup      6.6744000E-03    8.0310000E-03   -1.3566000E-03     1.000000    
 p142                 obsgroup      6.5405000E-03    7.9720000E-03   -1.4315000E-03     1.000000    
 p143                 obsgroup      6.3345000E-03    7.8550000E-03   -1.5205000E-03     1.000000    
 p144                 obsgroup      6.6435000E-03    7.7390000E-03   -1.0955000E-03     1.000000    
 p145                 obsgroup      6.2624000E-03    7.6810000E-03   -1.4186000E-03     1.000000    
 p146                 obsgroup      6.4169000E-03    7.4610000E-03   -1.0441000E-03     1.000000    
 p147                 obsgroup      6.3139000E-03    7.3450000E-03   -1.0311000E-03     1.000000    
 p148                 obsgroup      5.9122000E-03    7.2300000E-03   -1.3178000E-03     1.000000    
 p149                 obsgroup      6.3757000E-03    7.1130000E-03   -7.3730000E-04     1.000000    
 p150                 obsgroup      6.1697000E-03    6.7390000E-03   -5.6930000E-04     1.000000    
 p151                 obsgroup      5.9019000E-03    6.6270000E-03   -7.2510000E-04     1.000000    
 p152                 obsgroup      5.8504000E-03    6.5160000E-03   -6.6560000E-04     1.000000    
 p153                 obsgroup      5.8092000E-03    6.4070000E-03   -5.9780000E-04     1.000000    
 p154                 obsgroup      5.7062000E-03    6.3020000E-03   -5.9580000E-04     1.000000    
 p155                 obsgroup      5.3045000E-03    6.1970000E-03   -8.9250000E-04     1.000000    
 p156                 obsgroup      5.7371000E-03    6.0940000E-03   -3.5690000E-04     1.000000    
 p157                 obsgroup      5.5620000E-03    5.9920000E-03   -4.3000000E-04     1.000000    
 p158                 obsgroup      5.3766000E-03    5.8920000E-03   -5.1540000E-04     1.000000    
 p159                 obsgroup      5.4178000E-03    5.7910000E-03   -3.7320000E-04     1.000000    
 p160                 obsgroup      5.3663000E-03    5.6910000E-03   -3.2470000E-04     1.000000    
 p161                 obsgroup      5.2118000E-03    5.5950000E-03   -3.8320000E-04     1.000000    
 p162                 obsgroup      5.0058000E-03    5.4960000E-03   -4.9020000E-04     1.000000    
 p163                 obsgroup      5.1397000E-03    5.4480000E-03   -3.0830000E-04     1.000000    
 p164                 obsgroup      4.9543000E-03    5.3490000E-03   -3.9470000E-04     1.000000    
 p165                 obsgroup      4.8616000E-03    5.2500000E-03   -3.8840000E-04     1.000000    
 p166                 obsgroup      5.1397000E-03    5.2010000E-03   -6.1300000E-05     1.000000    
 p167                 obsgroup      4.7174000E-03    5.1040000E-03   -3.8660000E-04     1.000000    
 p168                 obsgroup      5.0161000E-03    5.0090000E-03    7.1000000E-06     1.000000    
 p169                 obsgroup      4.7483000E-03    4.9620000E-03   -2.1370000E-04     1.000000    
 p170                 obsgroup      4.7895000E-03    4.8690000E-03   -7.9500000E-05     1.000000    
 p171                 obsgroup      4.7174000E-03    4.7790000E-03   -6.1600000E-05     1.000000    
 p172                 obsgroup      4.6659000E-03    4.7360000E-03   -7.0100000E-05     1.000000    
 p173                 obsgroup      4.6968000E-03    4.6510000E-03    4.5800000E-05     1.000000    
 p174                 obsgroup      4.5938000E-03    4.5660000E-03    2.7800000E-05     1.000000    
 p175                 obsgroup      4.4187000E-03    4.5230000E-03   -1.0430000E-04     1.000000    
 p176                 obsgroup      4.4496000E-03    4.4400000E-03    9.6000000E-06     1.000000    
 p177                 obsgroup      4.5320000E-03    4.3580000E-03    1.7400000E-04     1.000000    
 p178                 obsgroup      4.4290000E-03    4.3170000E-03    1.1200000E-04     1.000000    
 p179                 obsgroup      4.3878000E-03    4.2370000E-03    1.5080000E-04     1.000000    
 p180                 obsgroup      4.4496000E-03    4.1550000E-03    2.9460000E-04     1.000000    
 p181                 obsgroup      4.3260000E-03    4.1160000E-03    2.1000000E-04     1.000000    
 p182                 obsgroup      4.0891000E-03    4.0400000E-03    4.9100000E-05     1.000000    
 p183                 obsgroup      4.4496000E-03    3.9620000E-03    4.8760000E-04     1.000000    
 p184                 obsgroup      4.1921000E-03    3.9230000E-03    2.6910000E-04     1.000000    
 p185                 obsgroup      4.1200000E-03    3.8480000E-03    2.7200000E-04     1.000000    
 p186                 obsgroup      4.0685000E-03    3.7740000E-03    2.9450000E-04     1.000000    
 p187                 obsgroup      3.9449000E-03    3.7010000E-03    2.4390000E-04     1.000000    
 p188                 obsgroup      3.8934000E-03    3.6290000E-03    2.6440000E-04     1.000000    
 p189                 obsgroup      4.0479000E-03    3.5610000E-03    4.8690000E-04     1.000000    
 p190                 obsgroup      4.0273000E-03    3.4960000E-03    5.3130000E-04     1.000000    
 p191                 obsgroup      3.7698000E-03    3.4300000E-03    3.3980000E-04     1.000000    
 p192                 obsgroup      3.7389000E-03    3.3670000E-03    3.7190000E-04     1.000000    
 p193                 obsgroup      3.7080000E-03    3.3340000E-03    3.7400000E-04     1.000000    
 p194                 obsgroup      3.6977000E-03    3.2700000E-03    4.2770000E-04     1.000000    
 p195                 obsgroup      3.6565000E-03    3.2080000E-03    4.4850000E-04     1.000000    
 p196                 obsgroup      3.6977000E-03    3.1780000E-03    5.1970000E-04     1.000000    
 p197                 obsgroup      3.7492000E-03    3.1200000E-03    6.2920000E-04     1.000000    
 p198                 obsgroup      3.6874000E-03    3.0630000E-03    6.2440000E-04     1.000000    
 p199                 obsgroup      3.6771000E-03    3.0050000E-03    6.7210000E-04     1.000000    
 p200                 obsgroup      3.5432000E-03    2.9470000E-03    5.9620000E-04     1.000000    
 p201                 obsgroup      3.3578000E-03    2.8890000E-03    4.6880000E-04     1.000000    
 p202                 obsgroup      3.3990000E-03    2.8620000E-03    5.3700000E-04     1.000000    
 p203                 obsgroup      3.3372000E-03    2.8070000E-03    5.3020000E-04     1.000000    
 p204                 obsgroup      3.3269000E-03    2.7530000E-03    5.7390000E-04     1.000000    
 p205                 obsgroup      3.4093000E-03    2.7000000E-03    7.0930000E-04     1.000000    
 p206                 obsgroup      3.2136000E-03    2.6500000E-03    5.6360000E-04     1.000000    
 p207                 obsgroup      3.2651000E-03    2.5990000E-03    6.6610000E-04     1.000000    
 p208                 obsgroup      3.2033000E-03    2.5510000E-03    6.5230000E-04     1.000000    
 p209                 obsgroup      3.2754000E-03    2.5040000E-03    7.7140000E-04     1.000000    
 p210                 obsgroup      3.2239000E-03    2.4610000E-03    7.6290000E-04     1.000000    
 p211                 obsgroup      2.9767000E-03    2.4170000E-03    5.5970000E-04     1.000000    
 p212                 obsgroup      3.1724000E-03    2.3740000E-03    7.9840000E-04     1.000000    
 p213                 obsgroup      3.1209000E-03    2.3300000E-03    7.9090000E-04     1.000000    
 p214                 obsgroup      3.0797000E-03    2.2870000E-03    7.9270000E-04     1.000000    
 p215                 obsgroup      2.9767000E-03    2.2440000E-03    7.3270000E-04     1.000000    
 p216                 obsgroup      2.8737000E-03    2.2000000E-03    6.7370000E-04     1.000000    
 p217                 obsgroup      2.9252000E-03    2.1570000E-03    7.6820000E-04     1.000000    
 p218                 obsgroup      2.8634000E-03    2.1150000E-03    7.4840000E-04     1.000000    
 p219                 obsgroup      2.9355000E-03    2.0760000E-03    8.5950000E-04     1.000000    
 p220                 obsgroup      2.7398000E-03    2.0560000E-03    6.8380000E-04     1.000000    
 p221                 obsgroup      2.8840000E-03    2.0180000E-03    8.6600000E-04     1.000000    
 p222                 obsgroup      2.8119000E-03    1.9790000E-03    8.3290000E-04     1.000000    
 p223                 obsgroup      2.7501000E-03    1.9420000E-03    8.0810000E-04     1.000000    
 p224                 obsgroup      2.7398000E-03    1.9050000E-03    8.3480000E-04     1.000000    
 p225                 obsgroup      2.6162000E-03    1.8700000E-03    7.4620000E-04     1.000000    
 p226                 obsgroup      2.7295000E-03    1.8350000E-03    8.9450000E-04     1.000000    
 p227                 obsgroup      2.5132000E-03    1.8000000E-03    7.1320000E-04     1.000000    
 p228                 obsgroup      2.5647000E-03    1.7660000E-03    7.9870000E-04     1.000000    
 p229                 obsgroup      2.5132000E-03    1.7340000E-03    7.7920000E-04     1.000000    
 p230                 obsgroup      2.5235000E-03    1.7010000E-03    8.2250000E-04     1.000000    
 p231                 obsgroup      2.4411000E-03    1.6690000E-03    7.7210000E-04     1.000000    
 p232                 obsgroup      2.6059000E-03    1.6370000E-03    9.6890000E-04     1.000000    
 p233                 obsgroup      2.5029000E-03    1.6050000E-03    8.9790000E-04     1.000000    
 p234                 obsgroup      2.4720000E-03    1.4480000E-03    1.0240000E-03     1.000000    
 p235                 obsgroup      2.5029000E-03    1.4210000E-03    1.0819000E-03     1.000000    
 p236                 obsgroup      2.4205000E-03    1.3940000E-03    1.0265000E-03     1.000000    
 p237                 obsgroup      2.4308000E-03    1.3680000E-03    1.0628000E-03     1.000000    
 p238                 obsgroup      2.4205000E-03    1.3310000E-03    1.0895000E-03     1.000000    
 p239                 obsgroup      2.1424000E-03    1.3050000E-03    8.3740000E-04     1.000000    
 p240                 obsgroup      2.3072000E-03    1.2800000E-03    1.0272000E-03     1.000000    
 p241                 obsgroup      2.1836000E-03    1.2680000E-03    9.1560000E-04     1.000000    
 p242                 obsgroup      2.3484000E-03    1.2430000E-03    1.1054000E-03     1.000000    
 p243                 obsgroup      2.3072000E-03    1.2190000E-03    1.0882000E-03     1.000000    
 p244                 obsgroup      2.2042000E-03    1.1740000E-03    1.0302000E-03     1.000000    
 p245                 obsgroup      2.1424000E-03    1.0950000E-03    1.0474000E-03     1.000000    
 p246                 obsgroup      2.1218000E-03    1.0850000E-03    1.0368000E-03     1.000000    
 p247                 obsgroup      2.2145000E-03    1.0640000E-03    1.1505000E-03     1.000000    
 p248                 obsgroup      2.1424000E-03    1.0440000E-03    1.0984000E-03     1.000000    
 p249                 obsgroup      1.9673000E-03    1.0240000E-03    9.4330000E-04     1.000000    
 p250                 obsgroup      1.9570000E-03    1.0040000E-03    9.5300000E-04     1.000000    
 p251                 obsgroup      1.9673000E-03    9.8400000E-04    9.8330000E-04     1.000000    
 p252                 obsgroup      2.0085000E-03    9.6300000E-04    1.0455000E-03     1.000000    
 p253                 obsgroup      1.8128000E-03    9.3300000E-04    8.7980000E-04     1.000000    
 p254                 obsgroup      1.7098000E-03    9.0500000E-04    8.0480000E-04     1.000000    
 p255                 obsgroup      1.6789000E-03    8.7800000E-04    8.0090000E-04     1.000000    
 p256                 obsgroup      1.4832000E-03    8.5300000E-04    6.3020000E-04     1.000000    
 p257                 obsgroup      1.4935000E-03    8.2700000E-04    6.6650000E-04     1.000000    
 p258                 obsgroup      1.5038000E-03    8.1100000E-04    6.9280000E-04     1.000000    
 p259                 obsgroup      1.5141000E-03    7.8800000E-04    7.2610000E-04     1.000000    
 p260                 obsgroup      1.4626000E-03    7.7300000E-04    6.8960000E-04     1.000000    
 p261                 obsgroup      1.3596000E-03    7.5200000E-04    6.0760000E-04     1.000000    
 p262                 obsgroup      1.3081000E-03    7.3000000E-04    5.7810000E-04     1.000000    
 p263                 obsgroup      1.2978000E-03    7.0900000E-04    5.8880000E-04     1.000000    
 p264                 obsgroup      1.2669000E-03    6.8900000E-04    5.7790000E-04     1.000000    
 p265                 obsgroup      1.2669000E-03    6.7000000E-04    5.9690000E-04     1.000000    
 p266                 obsgroup      1.1948000E-03    6.5000000E-04    5.4480000E-04     1.000000    
 p267                 obsgroup      1.0815000E-03    6.3100000E-04    4.5050000E-04     1.000000    
 p268                 obsgroup      1.1536000E-03    6.1200000E-04    5.4160000E-04     1.000000    
