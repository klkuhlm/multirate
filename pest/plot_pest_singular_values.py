import numpy as np
import matplotlib.pyplot as plt
from glob import glob
from matplotlib.font_manager import FontProperties

# smaller font for legends
LegFontP = FontProperties()
LegFontP.set_size('small')

def read_singular(fn):
    fh = open(fn,'r')
    lines = fh.readlines()
    n = int(lines[0].split()[0])
    return np.array([float(x) for x in lines[1:n+1]])
    

colors = {'B1':'red','B2':'green','B3a':'blue','B3b':'cyan',
          'B4':'magenta','B4B5B8':'orange','B5':'pink',
          'B7a':'brown','B7b':'black','B8':'gray'}

fig = plt.figure(figsize=(14,10))
ax = fig.add_subplot(311)
for d in sorted(glob('singleB*')):
    if 'B4B5B8' in d:
        continue
    s = read_singular(d+'/s.mat')
    s /= s[0]
    n = s.shape[0]
    name = d.replace('single','')
    label = (name.replace('B3a','B3Na').
            replace('B3b','B3U').replace('B7a','B7Na').
            replace('B7b','B7U'))
    ax.semilogy(np.arange(n)+1,s,color=colors[name],label=label)
ax.set_title('single rate model')
ax.set_ylabel('rel. sing. values')

ax = fig.add_subplot(312)
for d in glob('doubleB*'):
    if 'B4B5B8' in d:
        continue
    s = read_singular(d+'/s.mat')
    s /= s[0]
    n = s.shape[0]
    name = d.replace('double','')
    label = (name.replace('B3a','B3Na').
            replace('B3b','B3U').replace('B7a','B7Na').
            replace('B7b','B7U'))
    ax.semilogy(np.arange(n)+1,s,color=colors[name],label=label)
ax.set_ylim([1.0E-6,1])
ax.set_title('double rate model')
ax.set_ylabel('rel. sing. values')
ax.legend(loc=0,ncol=4,prop=LegFontP)

ax = fig.add_subplot(313)
for d in glob('multirate2B*'):
    if 'B4B5B8' in d:
        continue
    s = read_singular(d+'/s.mat')
    s /= s[0]
    n = s.shape[0]
    name = d.replace('multirate2','')
    label = (name.replace('B3a','B3Na').
            replace('B3b','B3U').replace('B7a','B7Na').
            replace('B7b','B7U'))
    ax.semilogy(np.arange(n)+1,s,color=colors[name],label=label)
ax.set_title('multirate model')
ax.set_ylabel('rel. sing. values')
ax.set_xlabel('singular value number')

plt.savefig('pest_singular_values_pest.png')


