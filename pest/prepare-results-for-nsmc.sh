#!/bin/bash

set -o nounset
set -o errexit

NRZN=500

# create a generic uncertainty file
cat > ./uncert.dat << EOF
START COVARIANCE_MATRIX
  file "cov.mat"
  variance_multiplier 500
END COVARIANCE_MATRIX
EOF

# after running pest in each directory
# 1) generate covariance matrix for parameters and
#    link to generic uncertainty file in parent dir
for d in multirate2B8-bounds
do 
    cd ${d}
    if [[ ${d} == multirate* ]]; then
	type=multirate2
    elif [[ ${d} == single* ]]; then
	type=single
    else
	type=double
    fi
    test=${d/${type}/} 
    # put optimal parameter values into new pest file
    parrep ${d}.par ${d}.pst ${type}-post-${test}.pst > /dev/null

    # link jacobian to a similar base name as new pest file
    ln -sf ${d}.jco ${type}-post-${test}.jco

    # create covariance matrices
    pcov2mat ${d}.mtt cov.mat > /dev/null

    ln -sf ../uncert.dat ./${d}.uncert

    # create response file for RANDPAR

##   for setup of normal distribution
    cat > ./randpar.in << EOF
$type-post-$test.pst
n
e
y
$d.uncert

$d-randpar-
$NRZN
4464446
EOF

### for uniform distribution
##    cat > ./randpar.in << EOF
##$type-post-$test.pst
##u
##
##$d-randpar-
##$NRZN
##4464446
##EOF


    # get number of parameters from  first entry in header 
    # row of covariance matrix
    npar=`sed '1q' cov.mat | awk '{print $1}'`

    
    #if [[ $d == *B[458] ]]; then
    #	    (( npar -= 2 ))
    #	else
    #	    (( npar -= 1 ))
    #fi
    (( npar -= 1 ))
    #
    #if (( npar < 2 )); then
    #	(( npar = 2 ))
    #fi

    echo $d $npar

    # create response file for PNULPAR
    cat > ./pnulpar.in << EOF 
$type-post-$test.pst
y
$npar
n
$d-randpar-
$d-pnulpar-
EOF

    cd ../
done


# 3) convert jacobian to matrix and generate SVD 
#    of jacobian to look at singular values

##for d in singleB* doubleB* multirate2B*
##do
##    cd ${d}
##    jco2mat ${d}.jco qx.mat
##    matsvd qx.mat u.mat s.mat v.mat
##    cd ../
##done
##
### 4) run python script to plot up singular values
##epd-python plot_pest_singular_values.py