#!/bin/bash

set -o nounset
set -o errexit

NRZN=500

export OMP_NUM_THREADS=4

multiphi='7.0E-04'
singlephi='5.0E-03'
doublephi='1.0E-03' 

for d in multirate2B8-bounds
do 
    cd ${d}
    # create parameter files based on normal
    # distribution around solution
    randpar < ./randpar.in > /dev/null
    
    # project these parameters onto null space
    pnulpar < ./pnulpar.in > /dev/null

    # make a template pest control file with parameters setup 
    # to just do one iteration with parameters suggested by
    # section 3.18.3 "What to do next" of April 2012 Pest Addendum (p 104)
    # 1) change NOPTMAX from 30 to 1 or 0
    # 2) add JACUPDATE = 999 (Broyden's updating not used before)
    # 3) change RLAMBDA1 from 5 to 1
    # 4) change PHIRATSUF from 0.3 to 0.001
    # NB: 2-4 really only make any difference if NOPTMAX is 1

    if [[ ${d} == multirate* ]]; then
	phithresh=${multiphi}
    elif [[ ${d} == single* ]]; then
	phithresh=${singlephi}
    else
	phithresh=${doublephi}
    fi

    # changing parameters on lines 6 and 9 of pest control file
    sed -e 's/5  2  0.3  0.03  10/1  2  0.001  0.03  10  999/' \
	-e "s/30  0.01  3  3  0.01  3/10  0.01  3  3  0.01  3  ${phithresh}/" \
	-e 's/restart/norestart/' <${d}.pst >${d}-nsmc-base.pst

    for i in {1..500}; do
	
	echo ${d} ${i}
        # put NSMC parameters into pest control file
	parrep ${d}-pnulpar-${i}.par ${d}-nsmc-base.pst ${d}-nsmc-tmp.pst > /dev/null

        # run pest
   	pest ${d}-nsmc-tmp.pst /i <<< ${d}.jco > /dev/null  # use old jacobian
	#pest ${d}-nsmc-tmp.pst > /dev/null  # do not use old jacobian

	# rename .rec files for saving (all other files will be overwritten next i)
	mv ${d}-nsmc-tmp.rec ${d}-nsmc-${i}.rec
        mv output.txt output1-${d}-${i}.txt
    done
    cd ../
done