\documentclass{article}
\usepackage{amsmath,amssymb,fullpage}
\usepackage{color}
\usepackage[dvips]{graphicx}
\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}

\title{Response to comments on ``Core-scale solute transport model
  selection using the Akaike Information Criterion and Null-space
  Monte Carlo analysis'' [Paper \#2012WR013212]}

\begin{document}
\maketitle

The comments of the editors and reviewers are addressed in this
document.  Comments are in smaller italic font, while our responses
are below each comment in standard font.

\section{Associate Editor}
\begin{itemize}
\item \textit{\footnotesize I have three main concerns that I share with the
    reviewers that I believe need to be addressed forcefully. First
    the paper is quite long, and can be reduced in length without harm
    by referring to the appropriate modeling and data-fusion papers
    that have appeared in WRR and/or elsewhere.}

  We have cut most of the mathematical derivation and heavily edited the text for brevity and clarity. Only the dimensionless equations from the original manuscript are retained. Without compromising the content and flow of the material presented, what were equations (1)--(20) in the original manuscript have been condensed to equations (1)--(7) in the revised manuscript, and what used to be equations (21)--(25) are now just equation (8). Additionally, the sections 3.2, 3.2.1, and 3.2.2 of the original manuscript have been condensed into section 3.2. The material that was in section 3.2.2 is now a single paragraph in section 3.2. The original submission was 47
  total pages, while the revision is $\sim 35$ pages.

\item \textit{\footnotesize Secondly, the model selection part (title and
    paper) should be clarified. This does not require null-space Monte
    Carlo analysis as standard optimization methods will
    suffice. Moreover, the dimensionality of the parameter estimation
    problem is quite low so the strengths of NSMC might not be fully
    exercised. This should be clarified in the paper.}

  We have stressed the reasons we used NSMC and DREAM as inputs to the
  model selection process (including $\mathrm{AIC_c}$). Estimates of structural
  error using an ensemble of calibrated models produces more
  meaningful model selection than making a choice based on a single
  ``optimal'' solution. Standard optimisation would yield only a single residual for each observation, whereas our use of NSMC and DREAM yielded a distribution of residuals at each observation. It is the bias in these distributions of residuals at different data point (or equivalently, at different measurement times) that are the subject of our work, not the residuals at all data points for a single optimal parameter set. As further stated in the manuscript, NSMC was used because DREAM can be quite computationally intensive, although the latter
is a more theoretically rigorous approach.

\item \textit{\footnotesize Finally, the choice of lognormally weighted rates
    is subject to debate. This approach introduces one more parameter
    (spread of rates) than the two added by the SRMT model (mean rate
    and immobile porosity), yet many other models exist, such as a
    2-parameter probability density function that can do the same
    job. Thus, why is the lognormal approach superior? And why not try
    other MRMT models that do not add parameters relative to the SRMT,
    such as the fractal model of Schumer et al. (WRR, 2003).}

  We have expanded the discussion and justification of this.  The
  lognormal distribution is a common choice with the multirate model,
  and is often used for other important parameters in hydrogeology.
  There are many potential models to test, but a proper analysis of
  this aspect is outside the scope of the present work. In our response to reviewer \# 2 we demonstrate that using a 1-parameter distibution in the MRMT model leads to poorer model fits to data. It should be noted that even single-parameter distributions such as the single-parameter exponential distribution $f_X(x) = \frac{1}{a} e^{-x/a}$ does have two distinct statistics, $\mu = a$ and $\sigma^2 = a^2$, the latter of which would be a measure of the spread of rates. The reason such a model yields a poorer fit to data than a two-parameter model is that the two statistics are not independent.

\item \textit{\footnotesize Finally, one reviewer comments that the DREAM
    analysis does not add anything useful to the paper. I disagree --
    I think it is an important part of the analysis as DREAM is a
    formal statistical method, whereas NSMC is not. Moreover, the
    DREAM posterior can have any functional shape -- it all depends on
    the model-data mismatch and choice of the likelihood
    function. Thus the DREAM posterior is not limited to Gaussianity.}

  We concur with the Associate Editor in this regard, and have retained the DREAM analysis in the revised manuscript, where the description is more succinct than in the original manuscript).

\end{itemize}

\section{Reviewer \#1}
\begin{itemize}

\item \textit{\footnotesize I have two principal technical concerns about the
    methods presented and conclusions drawn. My first concern is
    regarding the applicability of AIC metrics to inverse modeling of
    time-series datasets with strong temporal autocorrelation. In such
    datasets, the $n$ in Equation 27 is only loosely related to
    information content in the dataset. $N$ could increase ten-fold in
    this example (higher frequency sampling) with no increase
    whatsoever in information content of the data. I believe if $N \gg
    k$, the first term in Equation 27 will always dominate, so the
    model with the lowest residual variance will always be
    selected. If $N$ is 'artificially' large, as it might be in a time
    series with high autocorrelation, the AIC metric may not be
    appropriate. I suggest de-emphasizing the AIC model selection
    analysis, particularly in the title.} 
  
  We agree with the reviewer's insightful observation that for time series data with \textit{high autocorrelation}, $n \ggg k$ implies the penalty for model complexity is vanishingly small and the criterion reduces to a ranking of the  models by residual variance. This is only a problem however, when the increased number of observations does not increase the information contained in the observation about the estimated parameters (due to \textit{high autocorrelation}). Hence, we performed sidebar optimization with PEST using only 30 of the original 269 data in test B8 and obtained basically the same results, with AIC$_c$ ranking the multirate model highest (Figure \ref{fig:reduceddataB8}). This is because the estimation variance is always smallest for the multirate model, and artificially reducing $n$ only has a modest effect on the final outcome. We also contend that a large $n$ allows one to better capture the variability in the data due to random measurement error which are assumed to be Gaussian in minimization of the sum of squared residuals. Further, the number of parameters to be estimated increases by 2 from the single-porosity to the multirate model, whereas the estimation variance changes by a factor of about 2 ($7.6\times 10^{-6}$ to $3.2\times 10^{-7}$).
\begin{figure}[!htbp]
  \centering
  \includegraphics[height=0.35\textheight]{reducedB8.eps}
  \caption{\label{fig:reduceddataB8}Model fits to test B8 data where only 30 data points were used in the optimization. The $\mathrm{AIC_c}$ for each model is included in parenthesis.}
\end{figure}

  \textit{\footnotesize Likewise, residuals will have strong temporal
    autocorrelation and thus interpretations of their variance (and
    the $r^2$ of the model/data fit) violates basic assumptions of
    these statistical measures. These issues may also impact the
    presumed 'statistical rigor' on the parameter pdfs produced by
    DREAM (figure 13). A discussion of this issue is warranted. }
    
    Only the single-porosity model residuals show appreciable temporal autocorrelation, and less so for the double-porosity model. These model's computed responses show strong departure from observed behavior. As can be seen in Figure \ref{fig:residuals} below, the residuals obtained with the multirate model for the long tests (B4, B5 \& B8) show moderate temporal autocorrelation at very early time but are mostly randomly distributed about zero. The statistical rigor of DREAM does not depend on the distribution of the residuals but on the sampling of the parameter space for parameters that minimize the sum of squared residuals.
\begin{figure}[!htbp]
  \begin{center}
    \includegraphics[height=0.16\textheight]{residuals-B4.eps}
    \includegraphics[height=0.16\textheight]{residuals-B5.eps} 
    \includegraphics[height=0.16\textheight]{residuals-B8.eps}
    \hspace{0.25cm}
    \includegraphics[height=0.15\textheight]{residuals-B8-histogram.eps}
  \end{center}
  \caption{\label{fig:residuals}Temporal residuals of tests B4, B5 and B8 and the histogram of the test B8 residuals.}
\end{figure}

\item \textit{\footnotesize The second concern I have is the application of
    Null-space Monte Carlo. I find the analysis of parameter and
    prediction uncertainty using NSMC to be quite interesting and
    relevant to the issue at hand. However, I am less clear how NSMC
    was really used as a 'model selection' tool, as implied in the
    title and abstract. The PEST-based inverse model could have been
    used to compare residual bias of the three models. If residual
    bias was the criterion for model selection, what did NSMC provide
    that a good optimization algorithm could not? Residual bias and
    prediction uncertainty are two different measures; please clarify
    which was used.} 

  NSMC used to generate multiple optimal parameter sets and, correspondingly, multiple model fits to (or realizations that fit) the data yielding distributions of residuals for each data point. Regular optimization algorithms yield one optimal set of parameters and one residual value for each data point. NSMC \& DREAM yield distribution of residuals at each data point. It is the bias of the residual distribution at each data point during the elution wave that is used in our work as a measure of prediction uncertainty, not the bias of residuals at all data points for one optimal set of parameters. Also refer to the response to Associate Editor comment \#2 above.

  \textit{\footnotesize Another concern I have is the low dimensionality
    of the problem; hence, as the authors mention there might not be a
    'null space'. At minimum, the authors should discuss the criterion
    used to discriminate between solution space and null space and
    compare the dimensionality of each.}

  We agree the low-dimensional models do not necessarily have a null
  space for the given datasets we used.  We have added a statement
  indicating we used a single-dimension for the null spaces (page 15,
  line 280 of revised manuscript), since the singular values from the
  SVD of the Jacobian did not reveal a clear cutoff or drop, as is
  often seen in high-dimensional models.

\end{itemize}

\begin{enumerate}
\item \textit{\footnotesize The purpose of the modeling should be
    clarified. For example, on line 39 the authors refer to a need for
    models that 'best describe' transport. A purely empirical model
    (no physics) might be better able to fit the data and provide
    reliable predictions than a physics-based model. Are you seeking
    predictive capability or insights into the transport mechanisms?
    Please discuss, as clarifying this issue is particularly important
    for model selection. }
  
  In this particular case, we are seeking a physically based
  analytical transport model which best describes observations.  This
  is not a limitation of the approach; one of the candidate models
  could be purely statistical or empirical.  We did not consider a
  statistical model, because they are typically poor predictors, even
  if they may fit observed data well.  The breakthrough curves are
  complex enough that an empirical model would need a large number of
  degrees of freedom to match data as well as the multirate model. In
  this case $\Delta$AIC$_c$ would heavily penalize the model.

\item \textit{\footnotesize Line 81: Please explain why the double-porosity
    'single rate' model was applied rather than the distributed
    parameter model? }
  
  The double-porosity model is a common choice to model breakthrough
  of tracers in a core-scale experiment, and was chosen for this
  reason.  At the core-scale it is sufficient to use a lumped-parameter model as the spatial extent of the rock matrix is not significant to warrant the use of a distributed model.

\item \textit{\footnotesize Line 92. Delete the word 'kinetics'. }

  Deleted.

\item \textit{\footnotesize Line 122. If you can do so in one or two
    sentences, please describe how Lucero et al. (1998) defined
    'sufficient'. It is pertinent to the conclusions of your paper. }

  Lucero et al. (1998) was largely a report describing the experiments
  and reporting the data. They performed simple ``curve fitting''
  using an available double-porosity model, only plotting results in
  linear space (both time and concentration).  Due to this, the model
  fits were dominated by the peak concentration, and did not reveal the significant difference between the single-porosity and
  double-porosity results that is clearly discernible in log-log plots. The term ``sufficient'' in the context of Lucero et al. (1998) was used qualitatively to imply that the models captured peak breakthrough.

\item \textit{\footnotesize Line 137: is the phrase 'posterior MC' standard
    usage? Is it appropriate here? }

  The phrase indicates that NSMC analysis was performed post-calibration to randomly sample the parameter space near optimality for parameter sets that would still pass some tolerance value for calibration. 

\item \textit{\footnotesize Equations 1-25. } \textit{\footnotesize
    a. Consider reducing the number of equations by referring to
    previous papers when possible. Also, carefully check that all
    equation variables are defined immediately before or after their
    first use. I found a few exceptions (e.g. $R_{im}$, $R_m$ are
    defined much later than their first appearance line 157). }
    
  We have reduced the mathematical derivation to just the final
  dimensionless equations, initial and boundary conditions, and the solution. We have also made sure that all variables are defined as appropriate.
    
  \textit{\footnotesize b. Emphasize early in the text whether these
    equation apply to all 3 models or just the multi-rate
    model. Dispersion is mentioned (e.g. line 198) and I do not
    believe that applies to all 3 models. }  
  
We mention in the revised manuscript that Laplace transform of the memory function vanishes identically (i.e. $\bar{g}(\lambda_D) \equiv 0$) for single-porosity, whereas for double-porosity with single rate mass transfer, $\bar{g}(\lambda_D) = \omega_D/(s + \lambda_D + \omega_D)$. Dispersion applies to all three models. The distinction between the models is simply no immobile domain in single-porosity, single-rate mass exchange in double-porosity model, and random variable for mass exchange in multirate model.

\item \textit{\footnotesize Line 253. Replace 'proposed' with
    'propose'. Explain why the truncated form is more physically
    plausible. }

  ``Proposed'' does not appear in the revised manuscript, as this
  entire section has been heavily revised.  The truncated log-normal distribution is simply more general. It allows one to apply limit to permissible values of the Damk\"{o}hler-I number. If limits apply to the
  minimum and maximum Damk\"{o}hler-I number, this would be a more
  physically realistic distribution.  If no limits exist, the proposed
  distribution degenerates to the standard lognormal distribution. Bahr \& Rubin (1987) and Haggerty \& Gorelick (1995) have indicated that if this number is bigger than 100 the mass exchange is instantaneous and not rate limited.

\item \textit{\footnotesize Line 271. Replace 'longer' with 'longer in
    duration'. }

  Replaced.

\item \textit{\footnotesize Line 277. I presume by 'early' you mean 'time $< 1$
    PV'? Please specify. }

  Yes. ``Early breakthrough'' is accepted terminology in solute transport literature and as used in the manuscript implies appearance of significant tracer concentration in effluent for $\mathrm{PV} \ll 1$.

\item \textit{\footnotesize Line 279. The difference between porosity
    estimates is interesting. Please specify (briefly) how the
    'laboratory' estimate was derived. Given the differences in
    methods for estimating porosity (yours and theirs), do the
    differences make physical sense? }
  
  Laboratory-determined porosity is measured by standard laboratory techniques that determine the total void space
  in the sample. It represents the true total porosity of the sample. The values estimated from breakthrough data is strongly influenced by the presence of preferential flow and is typically smaller than the true total porosity due to existence of dead-end pores, etc.

\item \textit{\footnotesize Line 296. Is the issue uncertainty or temporal
    variability or both? }
  
  The issue is uncertainty in the injection concentration which was difficult to maintain constant over the duration of the injection pulse.  We have changed the text to indicate
  ``making reported injection concentrations values more uncertain
  than those for short pulse injection tests'' (page 10 line 176 of revised
  manuscript).

\item \textit{\footnotesize Were the data log transformed before the
    inversion? Why (or why not)? }

  Concentration data were not log transformed for use with PEST, but
  concentrations were log-transformed with DREAM (now indicated on
  page 17 line 325). PEST was able to find parameter values that minimize the objective function and corresponding model computed breakthrough that fit the data across all concentration and time scale even when data were not log-transformed. This was not the case with DREAM.

\item \textit{\footnotesize The PEST parameter estimation process should have
    provided parameter uncertainty statistics. Please discuss these,
    and if appropriate provide in Tables 1 and 2. }

  Yes, PEST reports 95\% confidence intervals and the estimation covariance matrix for evaluation of parameter estimation uncertainty. NSMC and DREAM provide an alternative for evaluating parameter estimation uncertainty analysis that does not rely upon linearizing assumptions used to compute the 95\% confidence intervals.

\item \textit{\footnotesize Line 405. Replace "because" with "if". It is not
    necessarily the case that more complex models will use the same
    dataset as simple models. It is often the case that more complex
    models can take advantage of more diverse datasets. }

  Changed ``because'' to ``when'' (page 17 line 343). In our case all
  models are using the same breakthrough data.

\item \textit{\footnotesize Figure 1. Is the mudstone and anhydrite part of
    the dolomite? Please clarify or delete these labels. }

  The mudstone and anhydrite are confining layers.  This is now
  indicated in the figure caption.

\item \textit{\footnotesize How were the 95\% confidence intervals shown in
    Figure 12 calculated? }
  
  The figure caption has been changed to reflect these are PEST-estimated confidence intervals. PEST estimates 95\% confidence intervals assuming a Gaussian posterior distribution of the parameter with known mean and variance using standard methods, viz. $$\mu \in \left[\tilde{\mu} - |z_{\alpha/2}|\frac{s}{\sqrt{n}}, \tilde{\mu} + |z_{\alpha/2}| \frac{s}{\sqrt{n}}\right]$$ where $\tilde{\mu}$ is the estimate of $\mu$, $s$ is sample variance, $n$ is sample size and $z_{\alpha/2}$ the value of the standard normal variate such that $P(Z>z_{\alpha/2}) = \alpha/2$.

\end{enumerate}

\section{Reviewer \#2}
\begin{itemize}
\item \textit{\footnotesize My only complaint is that the paper is overly
    long. There are no new theoretical developments, so that a large
    fraction of the paper can be reduced to single phrases, such as
    "We assume the multi-rate mass transfer model of Haggerty and
    Gorelick (WRR 1995)." Or "to incorporate parsimony into model
    fitness, we choose the information content measure within the AIC$_c$
    (see Poeter 2005, etc.)." Then the paper turns into a very clean
    and short technical note that focuses on the message (which is not
    present in the title): On the necessity (superiority?) of MRMT to
    predict transport in dolomite cores. }

  As indicated above, the manuscript has been revised substantially and is now much shorter with mathematical formulation and background material reduced to literature references.

\item \textit{\footnotesize I am also a bit mistrustful of the choice of
    lognormally weighted rates. This introduces one more parameter
    (spread of rates) than the two added by the SRMT model (mean rate
    and immobile porosity). But there are many, many other models that
    can do this: any 2-parameter pdf will do. Is the lognormal
    superior? Why not try other MRMT models that do not add parameters
    relative to the SRMT, such as the fractal model of Schumer et
    al. (WRR, 2003)? }
    
    The solution presented can admit any distribution. We concede that using a single parameter distribution such as the exponential model, may not yield results that are as good as those obtained with the two parameter distributions. We implemented the exponential model in our code. The results obtained were not as good as those obtained with the lognormal distribution (see Figure \ref{fig:exp} below). However, using two parameter gamma and beta distributions yielded results that are comparable to those obtained with the lognormal distribution. This simply indicate that it is best to use two-parameter (or more-parameter) distributions for multirate transport in Culebra core. The increase in parameters by 1 is really not a problem since even moving from a single- to a double-porosity model leads to an increase in parameters.
\begin{figure}[!htbp]
  \centering
  \includegraphics[height=0.35\textheight]{exp.eps}
  \caption{\label{fig:exp}Comparing fits of model computed
    breakthrough based on 1-parameter exponential distribution for
    $\omega_D$ to that based on lognormal distribution. The double-
    and single-porosity model results are included for comparison.}
\end{figure}

\item \textit{\footnotesize I am not an expert at PEST or DREAM, but I am not
    convinced that the parameter uncertainty should be Gaussian.} 
  
  The parameter distributions are not always Gaussian.  In other cores
  we analyzed using this same methodology, the distribution for
  dispersivity ($\alpha_L$) was approximately exponential, as
  determined by DREAM MCMC.

  \textit{\footnotesize I find that the DREAM analysis is an
    unnecessary add-on to the paper.}

  We are retaining the DREAM analysis, because we feel it is good to
  compare the PEST null-space Monte Carlo results against, since MCMC
  is an accepted rigorous way to estimate posterior parameter
  distributions.

\item \textit{\footnotesize Finally, I am not a fan of the
    nomenclature. "Single porosity" is fine, but both the single-rate
    and multi-rate are essentially double porosity. The MRMT is
    partitioned into regions with different uptake and release rates,
    but both models are mobile/immobile domain models. This is more
    concrete when viewed from the single particle approach, where the
    random walk model is a two state (mobile and immobile) Markov
    chain. The single rate and multi-rate differ only by the
    distribution of random waiting times. }

  In the revised manuscript we state explicitly that the multirate model is basically a double-porosity model, where the rate coefficient is a random variable. We nevertheless retain the nomenclature of double-porosity for the simple single-rate mass exchange model, and ``multirate'' to the model with an exchange rate defined by a distribution.

\end{itemize}

\begin{enumerate}
\item \textit{\footnotesize ll. 12, 57 and elsewhere: The MRMT may either be
    characterized as completely deterministic (as it is in this paper)
    or completely random, if represented by a single particle model
    (i.e., Benson and Meerschaert, WRR 2008). The author's MRMT model
    is a superposition of deterministic domains with deterministic
    mass transfer between. It is only a normalization by total
    porosity that the weights describing the domain porosities
    integrate to one, I suppose giving the impression that the
    porosities are random. In other words, any nonnegative function
    that integrates to unity is a pdf, but it does not necessarily
    describe any actual random variable. There are no random variables
    in the MRMT equations, and all porosities of all domains are
    always present. }

  The rate coefficient is a random variable. However, integration over the rate coefficient space yield the deterministic model which admits the deterministic parameters $\mu$ and $\sigma$, the statistics of $\omega_D$. The model is deterministic but the rate coefficient is a random variable. There are no random variables in the solution because we integrate over the random-variable-space in computing the memory function. There is a random variable ($\Omega_D$) in the governing equation, with distribution $P(\Omega_D \leq \omega_D)$. Just because all the marbles are present in the bag does not mean that their color is not a random variable.
  
\item \textit{\footnotesize l. 117: Perhaps the most recent study along these
    lines was by Major et al (WRR, 2011). }

 For the sake of brevity, we have removed the material to which this comment alludes.

\item \textit{\footnotesize Eq. 1: May as well just present the one equation
    with one dependent variable and the memory function and move on. }

  We have reduced the mathematical derivation significantly.

\item \textit{\footnotesize l. 239: This is a weak justification of using the
    log-normal. I would prefer to see a test of several viable weight
    functions. }

  We have added some discussion related to our choice of the lognormal
  distribution (see Associate Editor comment \#3 response).  A
  comparison of different distributions in the multirate model would
  be interesting, but off topic for the current manuscript.

\item \textit{\footnotesize l. 256: This choice of min and max rates
    basically makes (24) and (25) useless, just use (21). }

  We have removed the standard lognormal model and retained the truncated lognormal model since it is the more general formulation; the former is a special case of the latter.

\item \textit{\footnotesize Eq. 26: What are the weights on the squared
    errors? I assume you used the inverse of the variance of the
    concentrations, commonly taken as proportional to concentration,
    right? If you used an unweighted sum of squared errors, you must
    justify this choice. }
  
  The weights should really be the inverse of variance of the measurement error. Since these are not reported with the data analyzed in the manuscript, we simply set the weights to unity.
  
\end{enumerate}


\section{Reviewer \#3}
\textit{\footnotesize I have only a minor question I believe the authors
  could address to improve readability. I do not see the need to
  present first the full set of equations for the multirate model and
  then again in the dimensionless case. I believe it is enough with
  the latter set. The former could be deleted (my choice, since the
  paper is quite long) or else send to an Appendix. }

The manuscript has been shortened significantly, with most mathematical derivations and
background reduced to literature references (also refer to the first response to the Associate Editor).

\end{document}